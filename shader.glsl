#version 330
#define M_PI 3.14159
#ifdef VERTEX_SHADER
/*INPUTS*/
layout(location = 0)in vec3 position;
layout(location = 1)in vec3 normal;
layout(location = 2)in vec4 colors;

/*UNIFORMS*/
uniform mat4 mvpMatrix;
uniform mat4 mvMatrix;
uniform mat4 normalMatrix;
uniform mat4 model;
uniform mat4 lightMVP;

/*OUTPUTS*/
out vec3 vertex_position;
out vec3 vertex_normal;
out vec4 vertex_color;
out vec4 shadow_coord;

void main( )
{
    vertex_color = colors;
    vertex_position = (mvMatrix * vec4(position, 1)).xyz;
    vertex_normal = mat3(normalMatrix)*normal;
    shadow_coord = lightMVP * vec4(position, 1);
    gl_Position = mvpMatrix*vec4(position, 1);
}
#endif

#ifdef FRAGMENT_SHADER
/*INPUTS*/
in vec3 vertex_position;
in vec3 vertex_normal;
in vec4 vertex_color;
in vec4 shadow_coord;

/*UNIFORMS*/
uniform vec3 source;
uniform vec3 obs;
uniform sampler2D shadowMap;
uniform int shadowOn;

/*VARIABLES*/
/*Pourcentage specular*/
float kd = .35;
float m = 25.0;
float bias = 0.0001;

/*OUTPUTS*/
out vec4 color;

float getVisibility(vec4 shadow_coord)
{
    float visibility = 1.0;
    vec3 ProjCoords  = shadow_coord.xyz / shadow_coord.w;
    vec2 UVCoords;
    UVCoords.x = 0.5 * ProjCoords.x + 0.5;
    UVCoords.y = 0.5 * ProjCoords.y + 0.5;
    float z = 0.5 * ProjCoords.z + 0.5;
    float Depth = texture(shadowMap, UVCoords).x;
    if (Depth < (z - bias))
    {
        visibility = 0.3;
    }
    return visibility;
}
void main( )
{
    vec3 l = normalize(source - vertex_position);
    vec3 o = normalize(obs - vertex_position);
    vec3 h = normalize(0.5 * (o + l));
    vec3 color3 = vertex_color.xyz;
    float cos_theta_h = max(0, dot(vertex_normal, h));
    float cos_theta = max(0, dot(vertex_normal, l));
    float fr = (m + 1.) / (2.*M_PI) * pow(cos_theta_h, m);
    if(shadowOn == 1)
    {
        float v = getVisibility(shadow_coord);
        color = vec4(v*((1.-kd)*(fr*cos_theta)*color3 + (kd*cos_theta)*color3), 1);
    }
    else
    {
        color = vec4(((1.-kd)*(fr*cos_theta)*color3 + (kd*cos_theta)*color3), 1);
    }
}
#endif
