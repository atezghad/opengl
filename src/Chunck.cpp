#include "../include/MindCraft.hpp"

Chunk::Chunk(){}

void Chunk::bbox()
{
    bounds(_pmin, _pmax);
}

Point Chunk::pmin() const
{
    return _pmin;
}

Point Chunk::pmax() const
{
    return _pmax;
}
