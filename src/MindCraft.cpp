#include "../include/MindCraft.hpp"
#include "window.h"
#include "vec.h"
#include "mat.h"
#include "draw.h"
#include "program.h"
#include "uniforms.h"
#include "texture.h"
#include "orbiter.h"
#include "image_io.h"
#include "wavefront.h"

MindCraft::~MindCraft(){}

MindCraft::MindCraft(const int width, const int height, const int swidth, const int sheight,
                    const std::string &img_filename, const float &h_max, const bool shadow, const int major, const int minor) :
    App(width, height, major, minor), shadow_width(swidth), shadow_height(sheight), _h_max(h_max), _shadow(shadow)
{
    mesh = read_mesh("data/cube.obj");
    z = read_image(img_filename.c_str());
    shader = read_program("shader.glsl");
    lshader = read_program("lshader.glsl");
    program_print_errors(shader);
    program_print_errors(lshader);
    vertex_count= mesh.vertex_count();
}

void MindCraft::initMesh()
{
    all.create(GL_TRIANGLES);
    for(int i = 0; i < z.height(); ++i)
    {
        for(int j = 0; j < z.width(); ++j)
        {
           for(unsigned int c = 0; c < mesh.positions().size(); ++c)
           {
                all.color(z(j, i));
                all.normal(mesh.normals().at(c));
                all.vertex((mesh.positions().at(c).x + j), (mesh.positions().at(c).y + z(j, i).r*_h_max), (mesh.positions().at(c).z + i));
                all.texcoord(mesh.texcoords().at(c));
           }
        }
    }
    vertex_count= all.vertex_count();
}

int MindCraft::initBuffers()
{
    glGenSamplers(1, &color_sampler);
    glSamplerParameteri(color_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glSamplerParameteri(color_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glSamplerParameteri(color_sampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glSamplerParameteri(color_sampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    /**INITIALISATION DU VERTEX BUFFER**/
    glGenBuffers(1, &vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, all.vertex_buffer_size(), all.vertex_buffer(), GL_STATIC_DRAW);

    /**INITIALISATION DU NORMAL BUFFER**/
    glGenBuffers(1, &normal_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, normal_buffer);
    glBufferData(GL_ARRAY_BUFFER, all.normal_buffer_size(), all.normal_buffer(), GL_STATIC_DRAW);

    /**INITIALISATION DU COLOR BUFFER**/
    glGenBuffers(1, &color_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, color_buffer);
    glBufferData(GL_ARRAY_BUFFER, all.color_buffer_size(), all.color_buffer(), GL_STATIC_DRAW);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    /**CONFIGURATION DES BUFFERS**/
    GLint attribute= glGetAttribLocation(lshader, "position");
    if(attribute < 0)
        return -1;
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glVertexAttribPointer(attribute, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(attribute);

    attribute= glGetAttribLocation(shader, "position");
    if(attribute < 0)
        return -1;
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glVertexAttribPointer(attribute, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(attribute);

    attribute= glGetAttribLocation(shader, "normal");
    if(attribute < 0)
        return -1;
    glBindBuffer(GL_ARRAY_BUFFER, normal_buffer);
    glVertexAttribPointer(attribute, 3, GL_FLOAT, GL_FALSE,  0,  0);
    glEnableVertexAttribArray(attribute);

    attribute= glGetAttribLocation(shader, "colors");
    if(attribute < 0)
        return -1;
    glBindBuffer(GL_ARRAY_BUFFER, color_buffer);
    glVertexAttribPointer(attribute, 4, GL_FLOAT, GL_TRUE,  0,  0);
    glEnableVertexAttribArray(attribute);
    if(_shadow)
    {
        /**INITIALISATION DU ZBUFFER**/
        glGenTextures(1, &shadow_map);
        glBindTexture(GL_TEXTURE_2D, shadow_map);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, shadow_width, shadow_height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        /**INITIALISATION DU FRAMEBUFFER**/
        glGenFramebuffers(1, &zbuffer);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, zbuffer);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, shadow_map, 0);

        glDrawBuffer(GL_NONE);
        glReadBuffer(GL_NONE);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            return -1;
    }
    return 0;
}

void MindCraft::initChunks()
{
    int nb_chunk_x = z.width()/CHUNCK_SIZE;
    int nb_chunk_y = z.height()/CHUNCK_SIZE;

    for(int i = 0; i < nb_chunk_x; ++i)
    {
        for(int j = 0; j < nb_chunk_y; ++j)
        {
            Chunk c;
            c.create(GL_TRIANGLES);
            std::list<vec3> p;
            for(int x = i*CHUNCK_SIZE; x < i*CHUNCK_SIZE+CHUNCK_SIZE; ++x)
            {
                for(int y = j*CHUNCK_SIZE; y < j*CHUNCK_SIZE+CHUNCK_SIZE; ++y)
                {
                    for(auto it : mesh.positions())
                   {
                        c.color(z(y, x));
                        vec3 v(y, z(y, x).r*16.,  x);
                        c.vertex((it.x + v.x), (it.y + v.y), (it.z + v.z));
                        p.push_back(v);
                   }
                }
            }
            c.bbox();
            chuncks.push_back(c);
            positions.push_back(p);
        }
    }
}

int MindCraft::init()
{
    initMesh();
    if(initBuffers() != 0)
        return -1;
    Point pmin, pmax;
    all.bounds(pmin, pmax);
    camera.lookat(pmin, pmax);
    _light = Light(center(pmin, pmax));

    glClearColor(0.2f, 0.2f, 0.5f, 1.);
    glClearDepthf(1);
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    return 0;
}

void cleanAndMoveCameraViewer(Orbiter &camera)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    int mx, my;
    unsigned int mb = SDL_GetRelativeMouseState(&mx, &my);
    if(mb & SDL_BUTTON(1))
        camera.rotation(mx, my);
    else if(mb & SDL_BUTTON(3))
        camera.move(mx);
    else if(mb & SDL_BUTTON(2))
        camera.translation((float) mx / (float) window_width(), (float) my / (float) window_height());
}

int MindCraft::render()
{
    cleanAndMoveCameraViewer(camera);
    _light.moveAndRotateLight();
    glBindVertexArray(vao);
    Transform mvpL;
    if(_shadow)
    {
            glBindFramebuffer(GL_FRAMEBUFFER, zbuffer);
            glClearColor(0.0f, 0.0f, 0.0f, 1.);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glViewport(0, 0, shadow_width, shadow_height);
            glUseProgram(lshader);
            Transform p = _light.projection(shadow_width, shadow_height, 130.);
            Transform v = _light.view();
            mvpL = p * v;
            GLuint location = glGetUniformLocation(lshader, "mvpMatrix");
            glUniformMatrix4fv(location, 1, GL_TRUE, mvpL.buffer());
            glCullFace(GL_FRONT);
            glDrawArrays(GL_TRIANGLES, 0, vertex_count);
    }
    else
    {
        mvpL = Identity();
    }
    {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glClearColor(0.0f, 0.1f, 0.4f, 1.);
        glViewport(0, 0, window_width(), window_height());
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glUseProgram(shader);

        Transform p   = camera.projection(window_width(), window_height(), 65.);
        Transform v   = camera.view();
        Transform m   = Identity();
        Transform mvp = p * v * m;
        Transform mv  = v * m;

        GLuint location;

        location = glGetUniformLocation(shader, "mvpMatrix");
        glUniformMatrix4fv(location, 1, GL_TRUE, mvp.buffer());

        location = glGetUniformLocation(shader, "lightMVP");
        glUniformMatrix4fv(location, 1, GL_TRUE, mvpL.buffer());

        location = glGetUniformLocation(shader, "normalMatrix");
        glUniformMatrix4fv(location, 1, GL_TRUE, mv.normal().buffer());

        location = glGetUniformLocation(shader, "model");
        glUniformMatrix4fv(location, 1, GL_TRUE, m.buffer());

        location = glGetUniformLocation(shader, "mvMatrix");
        glUniformMatrix4fv(location, 1, GL_TRUE, mv.buffer());

        location = glGetUniformLocation(shader, "source");
        glUniform3f(location, _light.position().x, _light.position().y, _light.position().z);

        location = glGetUniformLocation(shader, "obs");
        glUniform3f(location, camera.position().x, camera.position().y, camera.position().z);

        location = glGetUniformLocation(shader, "shadowOn");
        glUniform1i(location, _shadow);

        location = glGetUniformLocation(shader, "shadowMap");
        glUniform1i( location, 0);

        if(_shadow)
        {
            glActiveTexture(GL_TEXTURE);
            glBindTexture(GL_TEXTURE_2D, shadow_map);
            glBindSampler(0, color_sampler);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, miplevels(shadow_width, shadow_height));
            glGenerateMipmap(GL_TEXTURE_2D);
            glCullFace(GL_BACK);
        }
        glDrawArrays(GL_TRIANGLES, 0, vertex_count);
    }
    return 1;
}

int MindCraft::quit()
{
    glDeleteTextures(1, &shadow_map);
    glDeleteFramebuffers(1, &zbuffer);
    glDeleteBuffers(1, &vertex_buffer);
    glDeleteBuffers(1, &normal_buffer);
    glDeleteBuffers(1, &color_buffer);
    release_program(lshader);
    release_program(shader);
    release_context(m_context);
    release_window(m_window);
    mesh.release();
    all.release();
    return 0;
}
