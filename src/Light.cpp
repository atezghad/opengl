#include "../include/MindCraft.hpp"

Light::Light(){}
Light::Light(const Point &target) : _target(target), _deltaR(1./15.), _deltaT(1./5.), _rx(0), _ry(0), _d(0)
{
    _spot.lookat(target, 150);
    _spot.rotation(25., 5.);
}

Point Light::position()
{
    return _spot.position();
}

void Light::moveAndRotateLight()
{
    float tx = 0, tz=0;
    if(key_state(SDLK_UP))
        tz+=_deltaT;
    if(key_state(SDLK_DOWN))
        tz-=_deltaT;
    if(key_state(SDLK_RIGHT))
        tx+=_deltaT;
    if(key_state(SDLK_LEFT))
        tx-=_deltaT;
    if(key_state(SDLK_KP_8))
        _ry+=_deltaR;
    if(key_state(SDLK_KP_2))
        _ry-=_deltaR;
    if(key_state(SDLK_KP_6))
        _rx+=_deltaR;
    if(key_state(SDLK_KP_4))
        _rx-=_deltaR;
    if(key_state(SDLK_KP_PLUS))
        _d++;
    else if(key_state(SDLK_KP_MINUS))
        _d--;

    _target = _target + Vector(tx, 0, tz);
    _spot.lookat(_target, 200);
    _spot.rotation(_rx, _ry);
    _spot.move(_d);
}

Transform Light::projection(const float &width, const float &height, const float &fov)
{
    return _spot.projection(width, height, fov);
}

Transform Light::view()
{
    return _spot.view();
}
