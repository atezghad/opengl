#ifndef MINDCRAFT_H
#define MINDCRAFT_H

#include <list>

#include "app.h"
#include "mesh.h"
#include "image.h"
#include "program.h"
#include "orbiter.h"

#define CHUNCK_SIZE 64

class Chunk : public Mesh
{
    public:
        Chunk();
        Point pmin() const;
        Point pmax() const;
        void bbox();

    protected:
        Point _pmin;
        Point _pmax;
        Point _pcenter;
};


class Light
{

        public:
                Light();
                Light(const Point &target);
                void moveAndRotateLight();
                Point position();
                Transform projection(const float &width, const float &height, const float &fov);
                Transform view();

        protected:
                Point _target;
                Orbiter _spot;
                float _deltaR;
                float _deltaT;
                float _rx;
                float _ry;
                float _d;

};

class MindCraft : public App
{
    public:
        MindCraft(const int width, const int height,  const int swidth, const int sheight,
                const std::string &img_filename, const float& h_max, const bool shadow=true, const int major= 3, const int minor= 3);
        virtual ~MindCraft();
        int init();
        void initMesh();
        int initBuffers();
        void initChunks();
        int quit();
        int render();

    protected:
        std::vector<Chunk> chuncks;
        std::vector< std::list<vec3> > positions;
        Light _light;
        Mesh mesh;
        Mesh all;
        Image z;
        Orbiter camera;
        GLuint shader;
        GLuint lshader;
        GLuint vao;
        GLuint vertex_buffer;
        GLuint color_buffer;
        GLuint normal_buffer;
        GLuint tex_buffer;
        GLuint zbuffer;
        GLuint color_sampler;
        GLuint shadow_map;
        int shadow_width;
        int shadow_height;
        float _h_max;
        unsigned int vertex_count;
        bool _shadow;

    private:
};

#endif // MINDCRAFT_H
