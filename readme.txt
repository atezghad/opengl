TEZGHADANTI ANIS 11207955

- Les diff�rents param�tres du constructeur MindCraft (tp2.cpp):
    * Les dimensions de la fen�tre
    * Les dimensions de la shadow map
    * La hauteur maximale du terrain
    * Boolean pour l'affichage des ombres (true par d�faut)
    
- Pour d�placer la lumi�re, se servir des fl�ches directionnelles
- Pour faire tourner la lumi�re, se servir du clavier num�rique (4,2,6,8)