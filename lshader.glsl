#version 330
#define M_PI 3.14159
#ifdef VERTEX_SHADER
/*INPUTS*/
in vec3 position;

/*UNIFORMS*/
uniform mat4 mvpMatrix;

void main( )
{
    gl_Position = mvpMatrix*vec4(position, 1);
}
#endif

#ifdef FRAGMENT_SHADER

layout(location = 0) out float depth;
void main( )
{
   float depth = gl_FragCoord.z;
}
#endif
